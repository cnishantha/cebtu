<style media="screen">
  .nc:hover p{
    color: white !important;
  }
  @media screen and (max-width: 991px) {
    .ncmvi{
      height: 430px !important;
    }
  }
</style>
<div class="row">

    <div id="news-item-wrapper" class="container text-center">
        @foreach($news as $new)
        <div class="card news-card nc ncmvi" style="height: 400px;">
            <div class="card-img " style="height: 232px;text-align: center;">
                <img src="{{URL::to('/')}}/news/mainimg/{{$new->main_image}}" alt="" />
            </div>
            <div class="card-body">
                <div>
                  <p class="card-title" title="{{$new->header_title}}" style="color: black;font-size:22px;">{{$new->header_title}}</p>
                </div>

                <p style="width: 235px;" class="card-text" title="{{$new->long_desc}}">{{$new->long_desc}}</p>
                <p class="card-text" title="{{$new->created_at}}">{{$new->created_at}}</p>
                <a target="" href="{{route('newsfront.show',$new->id)}}" class="btn btn-default">Read
                    More...</a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="site-pagination">
      {{ $news->links('vendor.pagination.custom') }}
    </div>

</div>
                  <!--div class="row">
                     @foreach($news as $new)


                     <div class="col-md-4 col-sm-6">
                        <div class="news-box">
                           <div class="new-thumb" style="height:250px;text-align: center;">
                              <span class="cat c1">{{$new->newstag}}</span>

							   		<img style="width:auto; height:50vh;" src="{{URL::to('/')}}/news/mainimg/{{$new->main_image}}" alt="">

                           </div>
                           <div class="new-txt">
                              <ul class="news-meta">
                                 <li>{{$new->created_at}}</li>

                              </ul>
							   <div style="height: 80px;">
                              <h6 style="font-size:16px;"><a href="{{route('newsfront.show',$new->id)}}">{{$new->header_title}}</a></h6>
							   </div>
                              <textarea style="background-color:white;" rows="8" class="form-control" disabled>
                    {{$new->long_desc}}
                </textarea>
                           </div>
                           <div class="news-box-f"><a href="{{route('newsfront.show',$new->id)}}"><i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                     </div>

                 @endforeach
               -->
