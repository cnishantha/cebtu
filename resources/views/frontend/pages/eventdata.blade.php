
`<style type="text/css">
    @media screen and (max-width: 991px) {
    .event-card{
      height: 465px !important;
    }
  }
</style>
<style media="screen">
  .ec:hover li{
    color: white !important;
  }
  .ec:hover p{
    color: white !important;
  }
</style>
   <div class="row">
          <div id="events-item-wrapper" class="container">
        @foreach($events as $event)
        <div class="card event-card ec " style="height: 440px;">
            <div class="card-img " style="height: 218px;text-align: center;">
                <img src="{{URL::to('/')}}/event/mainimg/{{$event->main_image}}" alt="" />
            </div>
            <div class="card-body">
                <div>
                  <p class="card-title" title="{{$event->header_title}}" style="color: black;font-size:16px;">{{$event->header_title}}</p>
                </div>
               <ul class="event-meta">
                  <li><i class="far fa-calendar-alt"></i><strong>Date</strong>
                     <?php $date = strtotime($event->e_date);
                     $newdate = date('d-F-Y',$date);
                     echo $newdate;?>
                  </li>
                  <li>
                     <i class="far fa-clock"></i><strong>Time</strong>{{$event->e_time}}
                  </li>
                  <li>
                     <i class="fa fa-home"></i><strong>Venue</strong> &nbsp;{{$event->e_venue}}
                  </li>
                  <li><i class="fa fa-user"></i><strong>Organizer</strong> &nbsp;{{$event->orgnizer}}</li>
               </ul>
               <div style="" class="text-center">
                  <a target="" href="{{route('eventfront.show',$event->id)}}" class="btn btn-default">Read
                    More...</a>
               </div>

            </div>
        </div>
        @endforeach
    </div>
      <div class="site-pagination">
                        {{$events->links('vendor.pagination.custom') }}
      </div>
   </div>
               <!--
                      @foreach($events as $event)

                     <div class="col-md-4 col-sm-6" style="border:1px solid #8A2424;">
                        <div class="event-post">
                           <div class="thumb" style="height:250px;text-align: center;">
							   <a href="{{route('eventfront.show',$event->id)}}">
								   <i class="fas fa-link"></i>
							   </a>
							   <img style="width:auto; height:50vh;" src="{{URL::to('/')}}/event/mainimg/{{$event->main_image}}" alt="">
							</div>
                           <div class="event-post-content">
                              <div class="event-post-txt">
								  <div style="height:30px;">
                                 <h5 style="font-size:16px;color:black;"><a href="{{route('eventfront.show',$event->id)}}">{{$event->header_title}}</a></h5>
								  </div>
                                 <ul class="event-meta">
                                    <li><i class="far fa-calendar-alt"></i><strong>Date</strong> &nbsp;&nbsp;&nbsp; <?php $date = strtotime($event->e_date);
										$newdate = date('d-F-Y',$date);
										echo $newdate;?></li>
                                    <li><i class="far fa-clock"></i><strong>Time</strong> &nbsp;&nbsp;&nbsp;{{$event->e_time}}</li>

									  <li><i class="fa fa-home"></i><strong>Venue</strong> &nbsp;{{$event->e_venue}}</li>
                              <li><i class="fa fa-user"></i><strong>Organizer</strong> &nbsp;{{$event->orgnizer}}</li>
                              <li class="text-center">
                                 <a style="width: 150px;background-color: #8A2424;color: white;margin-top: 20px;" class="btn btn-default " href="{{route('eventfront.show',$event->id)}}">Read...</a></li>
                                 </ul>
                              </div>
                              <div class="text-center">

                              </div>
                           </div>
                        </div>
                     </div>

                   @endforeach
                -->
