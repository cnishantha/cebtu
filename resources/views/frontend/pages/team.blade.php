@extends('frontend.layouts.front')
@section('title')
Staff
@endsection
@section('content')
         <!--Sub Header Start-->
         <!--section>
               <div class="subh">
                 
                     <img src="{{URL::asset('assets/images/subheaderbg.jpg')}}" >
                     <div class="top-left">
                        <h2>Staff</h2>
                     </div>
               </div>
            </section-->
   
   
  <section id="" class="wf100 h2-news-articles" style="padding-top: 10px;padding-bottom: 35px !important;">
      <div class="main-content pagebg" style="padding:10px 10px; ">
            <!--Events Start-->
            <div class="team-grid official-members">
               <div class="container">
                <div id="staff_data">
					@include('frontend.pages.teamdata')
				   </div>
               </div>
               <!--Team End--> 
            </div>
            <!--Main Content End--> 
         </div>
   </section>
         @endsection    