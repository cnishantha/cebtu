@extends('frontend.layouts.front')
@section('title')
Events
@endsection
@section('content')
<div class="main-content" style="padding: 5px 5px;">
   <div class="events-wrapper">
      <div class="container">
         <div id="event_data">
            @include('frontend/pages/eventdata')
         </div>
      </div>
   </div>
</div>
@endsection