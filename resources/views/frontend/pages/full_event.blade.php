@extends('frontend.layouts.front')
@section('title')
Event Details
@endsection
@section('content')
<!--div class="photo-gallery main-content overlayfix" style="padding-top: 20px;">
<section class="wf100" >
  <div class="container">
    @foreach($events as $event)
    <div  class="intro" style="padding-bottom:15px;">
      <h3  class="text-center">{{$event->header_title}}</h3>
    </div>
    <div class="row"></div>
    <div>
      <img src="{{URL::to('/')}}/event/mainimg/{{$event->main_image}}" style="width:auto;height:400px;">
      <div class="">
        <div class="form-group label-floating is-empty">
          <label class="control-label">Organizer</label>
          <input type="text" class="form-control" name="e_heading" value="{{$event->orgnizer}}" disabled>
        </div>
      </div>
    </div>
    <div class="row photos col-md-12" style="">
      @foreach (explode(",",$event->gallery_img) as $gallery_img)
      <div class="col-sm-3 col-md-2  item">
        <a  href="{{URL::to('/')}}/event/event_gallery_images/{{$gallery_img}}" data-lightbox="photos">
          <img style="width: 175px;height: 130px;padding-right: 1px;padding-top: 10px;padding-bottom: 20px"  class="img-fluid" src="{{URL::to('/')}}/event/event_gallery_images/{{$gallery_img}}">
        </a>
      </div>
      @endforeach
    </div>
    <div class="col-sm-10">
      <div class="form-group label-floating is-empty">
        <label class="control-label">Organizer</label>
        <input type="text" class="form-control" name="e_heading" value="{{$event->orgnizer}}" disabled>
      </div>
    </div>
    <div class="col-sm-10">
      <div class="form-group label-floating is-empty">
        <label class="control-label">Contact No</label>
        <input type="text" class="form-control" name="e_heading" value="{{$event->contactno}}" disabled>
      </div>
    </div>
    <div class="col-sm-10">
      <div class="form-group label-floating is-empty">
        <label class="control-label">Date</label>
        <input type="text" class="form-control" name="e_heading" value="{{$event->e_date}}" disabled>
      </div>
    </div>
    <div class="col-sm-10">
      <div class="form-group label-floating is-empty">
        <label class="control-label">Time</label>
        <input type="text" class="form-control" name="e_heading" value="{{$event->e_time}}" disabled>
      </div>
    </div>
    <div class="outro col-md-10" style="padding-top: 20px;">
      <textarea rows="4" class="form-control" disabled>
      {{$event->short_desc}}
      </textarea>
    </div>
    <div >
      <div class="outro col-md-10" style="padding-top: 20px;">
        <textarea rows="15" class="form-control" disabled>
        {{$event->long_desc}}
        </textarea>
      </div>
      <div class="col-sm-10">
        <div class="form-group label-floating is-empty">
          <label class="control-label">Video Link: </label>
          <a href="" target="_blank"></a>
        </div>
      </div>
      <div class="col-sm-10">
        <div class="form-group label-floating is-empty">
          <label class="control-label">Venue</label>
          <input type="text" class="form-control" name="e_heading" value="{{$event->e_venue}}" disabled>
        </div>
        <button  class="btn btn-warning pull-right" onclick="goBack()">Go Back</button>
      </div>
    </div>
    @endforeach
  </div>
</section>
</div-->
<style type="text/css">
.evtinfo ul.news-meta li{
font-size:14px;
}
.evtinfo ul.news-meta li strong{
color: #8A2424;;
}
.news-meta{
padding-top: 5px;
}
.e1{
padding-left: 5px;
}
</style>
<div class="main-content p80" style="padding-top: 25px !important;padding-bottom: 25px !important;">
<div class="news-details">
  <div class="container">
    <div class="row">
      <div class="">
        <div class="row">
          <div class="col-md-5 evtinfo">
            <div class="new-thumb"><a href="#"><i class="fas fa-link"></i></a> <span class="cat c4"></span> <img src="{{URL::to('/')}}/event/mainimg/{{$event->main_image}}" alt="">
          </div>
          <ul class="news-meta">
            <li><strong>Date: </strong>{{$event->e_date}}</li>
            <li><strong>Time: </strong>{{$event->e_time}}</li>
            <li><strong>Venue: </strong>{{$event->e_venue}}</li>
            <li><strong>Organizer: </strong>{{$event->orgnizer}}</li>
          </ul>
        </div>
        <div class="col-md-7">
          <div class="new-txt">
            <h4>{{$event->header_title}}</h4>
            <ul class="gallery-3-col">
              @foreach (explode(",",$event->gallery_img) as $gallery_img)
              <div class="col-md-2 col-sm-6 col-xs-4   item">
                <a  href="{{URL::to('/')}}/event/event_gallery_images/{{$gallery_img}}" data-lightbox="photos">
                  <img style="width: 75px;height: 60px;padding-right: 1px;padding-top: 10px;padding-bottom: 1px"  class="img-fluid" src="{{URL::to('/')}}/event/event_gallery_images/{{$gallery_img}}">
                </a>
              </div>
              @endforeach
            </ul>
            <div class="">
                <div class="cs-event-detail-holder">
                  <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                      <div class="cs-event-detail-cost">
                        <div class="cs-event-price">
                          <a data-fancybox="gallery" target="_blank"
                            href="https://www.youtube.com/watch?v=jsnUVV9rtjM">
                            <i class="fa fa-play-circle fa-4x" style="color: #8A2424;"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <br>
            <div class="" style="height: 180px;overflow-y: scroll;">
              <p align="justify"><strong style="color:black;">{{$event->short_desc}}</strong></p>
              <p align="justify" class="event-long-des">{!!$event->long_desc!!}</p>
            </div>
          </div>
          <button style="margin-right: 15px;"  class="btn btn-warning pull-right" onclick="goBack()">Go Back</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>


<script>
function goBack() {
window.history.back();
}
</script>
@endsection