<!--div class="row">

@foreach($homemembers as $member)
<div class="col-md-4 col-sm-6">
  <div class="h3-team-box">
    <div class="team-info">
      <h5>{{$member->name}}&nbsp;{{$member->lname}}</h5>
      <p>Employee ID:{{$member->employee_id}}<br>
      </p>
    </div>
    <?php if(empty($member->profile_img)){ ?>
    <img style="height:410px;width:340px;" src="{{URL::to('/')}}/members/profileimg/male-avatar.png" alt="">
    <?php }else{?>
    <img style="height:410px;width:340px;" src="{{URL::to('/')}}/members/profileimg/{{$member->profile_img}}" alt="">
    <?php }?>
  </div>
</div>
@endforeach
<div class="site-pagination">
  {{ $homemembers->links('vendor.pagination.custom') }}
</div>
</div-->
<style type="text/css">
@media screen and (min-width: 991px) {
.event-card{
height: 400px !important;
}
.uimg{
width:240px !important;
}
}
@media screen and (max-width: 991px) {
.dfimg{
text-align:center !important;
padding-top: 0px !important;
}
.uimg{
text-align:center !important;


}

}
</style>
<style media="screen">
  .dts p{
    color: black;
  }
  .mbr:hover{
    background-color: #8A2424 !important;

  }
  .dts:hover p{
    color: white !important;
  }

</style>
<!--div class="row">
<div id="events-item-wrapper" class="container">
@foreach($homemembers as $member)

<div class="card event-card" style="height: 435px;">
  <div class="card-img " style="height: 320px;text-align: center;">
    <?php if(empty($member->profile_img)){ ?>
    <img class="dfimg" style="padding-top: 60px;" src="{{URL::to('/')}}/members/profileimg/male-avatar.png" alt="">
    <?php }else{?>
    <img class="uimg" style="" src="{{URL::to('/')}}/members/profileimg/{{$member->profile_img}}" alt="">
    <?php }?>
  </div>
  <div class="card-body text-center">
    <div>
      <h6 class="card-title" title="{{$member->name}}" style="color: black;">{{$member->name}}</h6>
    </div>
    <span style="">EmployeeID:<strong>{{$member->employee_id}}</strong> </span>
  </div>
</div>
@endforeach
</div>
<div class="site-pagination">
{{ $homemembers->links('vendor.pagination.custom') }}
</div>
</div-->
<div class="row">
  @foreach($homemembers as $member)
<div class="col-xs-12 col-sm-12 col-md-4 member-hover ">
<div class="well well-sm mbr" >
  <div class="row ft" >
    <div class="col-sm-4 col-xs-4 col-md-4 dts" >
      <?php if(empty($member->profile_img)){ ?>
      <img class="dfimg" style="height: 85px;" src="{{URL::to('/')}}/members/profileimg/male-avatar.png" alt="">
      <?php }else{?>
      <img style="height: 85px;" class="" style="" src="{{URL::to('/')}}/members/profileimg/{{$member->profile_img}}" alt="">
      <?php }?>
    </div>
    <div class="col-sm-8 col-xs-8 col-md-8 dts" style="padding-top: 15px;">
      <p>{{$member->name}}</p>
      <p>{{$member->employee_id}}</p>
    </div>
  </div>
</div>
</div>
@endforeach
<div class="site-pagination">
{{ $homemembers->links('vendor.pagination.custom') }}
</div>
</div>
