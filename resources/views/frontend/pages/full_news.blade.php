@extends('frontend.layouts.front')
@section('title')
News Details
@endsection
@section('content')
<!--div class="photo-gallery main-content overlayfix">
  <section class="wf100 p80 ">
  <div class="container">
          @foreach($news as $new)
    <div class="intro">
      <h3 class="text-center">{{$new->header_title}}</h3>
      <p class="text-center"> </p>
    </div>
          <div class="outro col-md-10" style="padding-top: 20px;">
      <div class="form-group label-floating is-empty">
        <label class="control-label">News Tag</label>
        <input type="text" class="form-control" name="e_heading" value="{{$new->newstag}}" disabled>
      </div>
    </div>
    <div class="outro col-md-10" style="padding-top: 20px;">
      
      <textarea rows="15" class="form-control" disabled>
      {{$new->long_desc}}
      </textarea>
    </div>
    
    <div class="outro col-md-10" style="padding-top: 20px;">
      <div class="form-group label-floating is-empty">
        <label class="control-label">Video Link: </label>
        <a href="{{$new->video}}" target="_blank">{{$new->video}}</a>
        
        
      </div>
    </div>
    <div class="row photos col-md-12">
              @foreach (explode(",",$new->gallery_img) as $gallery_img)
      
      
      <div class="col-sm-3 col-md-2  item">
        <a  href="{{URL::to('/')}}/news/news_gallery_images/{{$gallery_img}}" data-lightbox="photos">
          <img style="width: 175px;height: 130px;padding-right: 1px;padding-top: 10px;"  class="img-fluid" src="{{URL::to('/')}}/news/news_gallery_images/{{$gallery_img}}">
        </a>
      </div>
      @endforeach
      
      
    </div>
    <div class="col-sm-10">
      <button  class="btn btn-warning pull-right" onclick="goBack()">Go Back</button>
      
    </div> 
    @endforeach
  </div>
  </section>
</div-->
<style type="text/css">
.evtinfo ul.news-meta li{
font-size:14px;
}
.evtinfo ul.news-meta li strong{
color: #8A2424;;
}
.news-meta{
padding-top: 5px;
}
.e1{
padding-left: 5px;
}
</style>
<div class="main-content p80" style="padding-top: 25px !important;padding-bottom: 25px !important;">
<div class="news-details">
  <div class="container">
    <div class="row">
      <div class="">
        <div class="row">
          <div class="col-md-6 evtinfo">
            <div class="new-thumb"><a href="#"><i class="fas fa-link"></i></a> <span class="cat c4"></span> <img src="{{URL::to('/')}}/news/mainimg/{{$new->main_image}}" alt="">
          </div>
          <ul class="news-meta">
            <li><strong>Date: </strong></li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="new-txt">
            <h4>{{$new->header_title}}</h4>
            <ul class="gallery-3-col">
              @foreach (explode(",",$new->gallery_img) as $gallery_img)
              <div class="col-md-2 col-sm-6 col-xs-6  item">
                <a  href="{{URL::to('/')}}/news/news_gallery_images/{{$gallery_img}}" data-lightbox="photos">
                  <img style="width: 75px;height: 60px;padding-right: 1px;padding-top: 10px;"  class="img-fluid" src="{{URL::to('/')}}/news/news_gallery_images/{{$gallery_img}}">
                </a>
              </div>
              @endforeach
            </ul>      
              <div class="cs-event-detail-holder">
                <div class="row">
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="cs-event-detail-cost">
                      <div class="cs-event-price">
                        <a data-fancybox="gallery" target="_blank"
                          href="{{$new->video}}">
                          <i class="fa fa-play-circle fa-4x" style="color: #8A2424;"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <br>
            <div style="height: 180px;overflow-y: scroll;">
              <p align="justify" class="e1">{{$new->long_desc}}</p>
            </div>            
          </div>
          <button style="margin-right: 15px;" class="btn btn-warning pull-right" onclick="goBack()">Go Back</button>
        </div>
      </div>    
    </div>
  </div>
</div>
</div>
</div>

<script>
function goBack() {
window.history.back();
}
</script>

@endsection