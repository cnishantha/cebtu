@extends('frontend.layouts.front')
@section('title')
Members
@endsection
@section('content')

      
        
  <section id="" class="wf100  h2-news-articles" style="padding:20px 10px;">
      <div class="main-content pagebg ">
            <div class="team-grid official-members" >
               <div class="container">
               <div class="row">      
               <div class="col-md-3" >
                    <h5>Search Members</h5><br>
                        <div class="" >
                            <input class="form-control" name="name" type="text" placeholder="Search Members" />
                        </div> <br>
                    <div class="widget cs-text-widget">
                        <div class="cs-text">
                            <h5>Sort Members </h5><br>
                            <a href="#" class="btn btn-default form-control">Name</a><br><br>
                            <a href="#" class="btn btn-default form-control">Employee ID</a><br><br>
                        </div>
                    </div>
               </div>
                <div class="col-md-9" id="members_data">
                  <h5>Members Directory</h5><br>
               @include('frontend.pages.membersdata')
               </div>
               </div>

               </div>
               <!--Team End--> 
            </div>
            <!--Main Content End--> 
         </div>
   </section>
         @endsection    