<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

/*Route::get('/', function () {
    return view('frontend/pages/index');
});*/
Route::get('wel', function () {
    return view('frontend/pages/wel');
});

/*Route::get('myevents', function () {
    return view('user/pages/my_events');
});*/
Route::get('/', 'HomefrontController@index')->name('webfront');

Route::get('/about', function () {
    return view('frontend/pages/aboutus');
});

Route::get('/about', function () {
    return view('frontend/pages/aboutus');
});

Route::get('/contactinfo', function () {
    return view('frontend/pages/contact-information');
});

/*Route::get('/contact', function () {
    return view('frontend/pages/contact');
});*/

/*Route::get('/members', function () {
    return view('frontend/pages/members');
});*/
Route::get('/onlineregister', function () {
    return view('frontend/pages/new_member_reg');
});
Route::get('/contact', 'ContactfrontController@index')->name('contactfront.show');
Route::post('/contact', 'ContactfrontController@store')->name('contactfront.send');
Route::get('/ourstaff', 'StafffrontController@index')->name('stafffront');
Route::get('/ourstaff/fetch_data', 'StafffrontController@fetch_data')->name('stafffrontdata');
Route::get('/search','SearchController@noResult')->name('searchnoresult');
Route::post('/search','SearchController@SearchAll')->name('searchall');
Route::get('/noresult','SearchController@noResult')->name('noresult');

/*Route::get('/ourstaff', function () {
    return view('frontend/pages/team');
});*/

/*Route::post('/search',function(){
   $q = Input::get('q');
    


   if ($q!=""){
   	$member = DB::select("select * from users where employee_id  like ? or name  like ? ",['%'.$q.'%','%'.$q.'%']);
    $downloads = DB::select("select * from downloads where name  like ? or description  like ? ",['%'.$q.'%','%'.$q.'%']);

   	if(count($member)>0||count($downloads)){
   		return view('frontend/pages/search')->withDetails($member,$downloads)->withQuery($q);
   }

   	}
   	return view('frontend/pages/no_result');
});*/

Route::get('/sendbirthdaywish', 'BirthdayMessageController@sendBirthdaymessage')->name('birthdaywish');

Route::get('/ournews', 'NewsfrontController@index')->name('news');
Route::get('ournews/{id}','NewsfrontController@show')->name('newsfront.show');
Route::get('/ournews/fetch_data', 'NewsfrontController@fetch_data')->name('newsfrontdata');
Route::get('/ourevents', 'EventfrontController@index')->name('events');
Route::get('/downloads', 'DownloadsfrontController@index')->name('downloads');
Route::get('/downloads/fetch_data', 'DownloadsfrontController@fetch_data')->name('downloadfrontdata');
Route::get('/ourmembers', 'MembersfrontController@index')->name('members');
Route::get('/ourmembers/fetch_data', 'MembersfrontController@fetch_data')->name('membersfrontdata');
Route::get('ourevents/{id}','EventfrontController@show')->name('eventfront.show');
Route::get('/ourevents/fetch_data', 'EventfrontController@fetch_data')->name('eventfrontdata');

//Route::get('/dashboard/admin','AdminController@index')->name('adminhome');
//Route::get('/dashboard/user','HomeController@index')->name('userhome');



//Route::get('/adminlogin','SessionController@create')->name('adminlogin');
//Route::get('/adminsignup','RegisterController@create');


//Route::post('/adminsignup','RegisterController@store');

//Route::get('/adminlogout','SessionController@destroy');

//Route::post('/adminlogin','SessionController@store');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/myevents/', 'HomeController@showevents')->name('myevents');
Route::post('/mymessages/', 'HomeController@showmessages')->name('mymessages');
Route::post('/mygroups/', 'HomeController@mygroups')->name('mygroups');
Route::get('user/editprofile','HomeController@showeditprofile')->name('user.editprofile');
Route::post('user/editprofile','HomeController@saveprofile')->name('user.editprofile');
Route::get('user/changepassword','HomeController@changepasswordform')->name('user.changepassword');
Route::post('user/changepassword','HomeController@savechangepassword')->name('user.changepassword');
Route::get('user/profile','HomeController@viewprofile')->name('user.profile'); 

Route::get('admin/editprofile','AdminController@showeditprofile')->name('admin.editprofile');
Route::post('admin/editprofile','AdminController@saveprofile')->name('admin.editprofile');
Route::get('admin/changepassword','AdminController@changepasswordform')->name('admin.changepassword');
Route::post('admin/changepassword','AdminController@savechangepassword')->name('admin.changepassword');
Route::get('admin/profile','AdminController@viewprofile')->name('admin.profile'); 
Route::get('admin/home','AdminController@index')->name('admin.home'); 
Route::get('admin/editor','EditorController@index'); 
Route::get('admin/test','EditorController@test');

//Export Excel Report Section Routes
Route::get('admin/reports/usersbybday','ReportController@getUsersbyBdayForm')->name('reports.exportbybday');
Route::post('admin/reports/usersbybday','ReportController@getUsersbyBday')->name('reports.exportbybday'); 

Route::get('admin/reports/usersbyworkplace','ReportController@getUsersbyWorkplaceForm')->name('reports.exportbyworkplace');
Route::post('admin/reports/usersbyworkplace','ReportController@getUsersbyWorkplace')->name('reports.exportbyworkplace'); 

Route::get('admin/reports/usersbydesignation','ReportController@getUsersbyDesignationForm')->name('reports.exportbydesignation');
Route::post('admin/reports/usersbydesignation','ReportController@getUsersbyDesignation')->name('reports.exportbydesignation'); 

Route::get('admin/reports/usersbygroup','ReportController@getUsersbyGroupForm')->name('reports.exportbygroup');
Route::post('admin/reports/usersbygroup','ReportController@getUsersbyGroup')->name('reports.exportbygroup'); 


//Pdf Report Section Routes

Route::get('admin/pdfreports/','PdfReportController@getPdfReportsPage')->name('pdfreports.exportpdfs');
Route::get('admin/pdfreports/exportusersbyworkplace','PdfReportController@exportuserbyworkplacepdf')->name('pdfreports.exportworkplacepdf');
Route::get('admin/pdfreports/exportusersbydesignation','PdfReportController@exportuserbydesignationpdf')->name('pdfreports.exportdesignationpdf');
Route::get('admin/pdfreports/exportusersbygroup','PdfReportController@exportuserbygrouppdf')->name('pdfreports.exportgrouppdf');
Route::get('admin/pdfreports/exportuser','PdfReportController@exportuserpdf')->name('pdfreports.exportuserpdf');




//test word route
Route::get('admin/createword','WordReportController@createWordDocx');


//Word Report Section Routes

Route::get('admin/wordreports','WordReportsController@getWordReportsPage')->name('wordreports.exportdocx');;
Route::get('admin/wordreports/exportusersbyworkplace','WordReportsController@exportuserbyworkplaceword')->name('wordreports.exportworkplaceword');
Route::get('admin/wordreports/exportusersbydesignation','WordReportsController@exportuserbydesignationword')->name('wordreports.exportdesignationword');
Route::get('admin/wordreports/exportusersbygroup','WordReportsController@exportuserbygroupword')->name('wordreports.exportgroupword');
Route::get('admin/wordreports/exportuser','WordReportsController@exportuserword')->name('wordreports.exportuserword');


//Membership section routes 
Route::delete('deleteall','MembershipController@deleteAll');
Route::delete('deleteallimported','MembershipController@deleteAllimported');
Route::delete('restoreall','MembershipController@restoreall');
Route::delete('recycleAll','MembershipController@recycleAll');
Route::get('admin/deletedmembers','MembershipController@showTrash')->name('member.deletedmembers'); 
Route::get('admin/members','MembershipController@index')->name('member.members'); 
Route::get('admin/members/add','MembershipController@create')->name('member.add');
Route::post('admin/members/add','MembershipController@store')->name('member.add'); 
Route::get('admin/members/importmemberlist','MembershipController@createimport')->name('member.import');
Route::post('admin/members/importmemberlist','MembershipController@storeimport')->name('member.import'); 
Route::get('admin/members/edit','MembershipController@edit')->name('member.edit');
Route::get('admin/members/restore/{id}','MembershipController@restore')->name('member.restore');
Route::get('admin/members/recycle/{id}','MembershipController@recycle')->name('member.recycle');
Route::get('admin/members/deleteimported/{id}','MembershipController@destroyimported')->name('member.deleteimported');
Route::get('admin/members/profile/{id}','MembershipController@show')->name('member.show');
Route::post('admin/members/edit/{id}','MembershipController@update')->name('member.edit'); 
Route::resource('members','MembershipController');

//online member registration routes
Route::get('/onlineregister','OnlineMemberRegisterController@create')->name('onlinemember.add');
Route::post('/onlineregister','OnlineMemberRegisterController@store')->name('onlinemember.add');


//Transfer Board Section Routes

Route::get('admin/transfersboard/import','TransferBoardController@createimport')->name('transfer.import');
Route::post('admin/transfersboard/import','TransferBoardController@storeimport')->name('transfer.import');
Route::get('admin/transfersboard/deleteimported/{id}','TransferBoardController@destroyimported')->name('transfer.deleteimported');

Route::get('admin/transfersboard/allrequests','TransferBoardController@allTransferRequests')->name('transfer.allrequests');
Route::get('admin/transfersboard/pendingrequests','TransferBoardController@pendingTransferRequests')->name('transfer.pendingrequests');
Route::get('admin/transfersboard/completedrequests','TransferBoardController@completedTransferRequests')->name('transfer.completedrequests');
Route::get('admin/transfersboard/addnewrequest','TransferBoardController@getaddtransferrequest')->name('transfer.addrequest');
Route::post('admin/transfersboard/addnewrequest','TransferBoardController@addtransferRequest')->name('transfer.addrequest');

Route::get('admin/transfersboard/edittransferrequest/{id}','TransferBoardController@getedittransferrequest')->name('transfer.editrequest');
Route::post('admin/transfersboard/edittransferrequest/{id}','TransferBoardController@edittransferRequest')->name('transfer.editrequest');
Route::get('admin/transfersboard/transferrequestinfo/{id}','TransferBoardController@transferProfile')->name('transfer.requestinfo');


Route::get('admin/transfersboard/transferrequest/{id}','TransferBoardController@transferInfoProfile')->name('transfer.requestcomplete');
Route::post('admin/transfersboard/transferrequest/{id}','TransferBoardController@completeTransferRequest')->name('transfer.requestcomplete');

Route::get('admin/transfersboard/transfercycles','TransferBoardController@allTransferCycles')->name('transfer.allcycles');
Route::get('admin/transfersboard/transfercycleinfo/{id}','TransferBoardController@getCycleInfo')->name('transfer.cycleinfo');
Route::get('admin/transfersboard/transfercycleinfoview/{id}','TransferBoardController@getCycleInfoview')->name('transfer.cycleinfoview');


Route::get('admin/transfersboard/completedtransfercycles','TransferBoardController@completedTransferCycles')->name('transfer.completedcycles');
Route::delete('deletealltransferrequests','TransferBoardController@deleteAllimported');
Route::get('admin/transfersboard/deletetransfercycle/{id}','TransferBoardController@deleteCycle')->name('transfer.deletecycle');
Route::delete('deleteallcycles','TransferBoardController@deleteAllCycles')->name('transfer.deleteAllcycles');

Route::get('admin/transfersboard/exportcyclestoexcel/{id}','TransferBoardController@ExportTransferCycleToExcel')->name('transfer.exportcycleexcel');
Route::get('admin/transfersboard/exportcyclestoword/{id}','TransferBoardController@ExportCyclesToWordDocx')->name('transfer.exportcycleword');
Route::get('admin/transfersboard/exportcyclestopdf/{id}','TransferBoardController@ExportCycleToPDF')->name('transfer.exportcyclepdf');
Route::resource('transfersboard','TransferBoardController');




//Contactbook section routes 
Route::delete('deleteallcontacts','ContactsController@deleteAll');
Route::delete('restoreallcontacts','ContactsController@restoreall');
Route::delete('recycleAllcontacts','ContactsController@recycleAll');
Route::get('admin/deletedcontacts','ContactsController@showTrash')->name('contact.deletedcontacts'); 
Route::get('admin/contacts','ContactsController@index')->name('contact.members'); 
Route::get('admin/contacts/add','ContactsController@create')->name('contact.add');
Route::post('admin/contacts/add','ContactsController@store')->name('contact.add'); 
Route::get('admin/contacts/edit','ContactsController@edit')->name('contact.edit');
Route::get('admin/contacts/restore/{id}','ContactsController@restore')->name('contact.restore');
Route::get('admin/contacts/recycle/{id}','ContactsController@recycle')->name('contact.recycle');
Route::get('admin/contacts/profile/{id}','ContactsController@show')->name('contact.show');
Route::post('admin/contacts/edit/{id}','ContactsController@update')->name('contact.edit'); 
Route::resource('contacts','ContactsController');

//Downloaditems section routes 
Route::delete('deletealldownloads','DownloadItemsController@deleteAll');
Route::delete('restorealldownloads','DownloadItemsController@restoreall');
Route::delete('recycleAlldownloads','DownloadItemsController@recycleAll');
Route::get('admin/deleteddownloaditems','DownloadItemsController@showTrash')->name('downloaditems.deleteditems'); 
Route::get('admin/downloaditems','DownloadItemsController@index')->name('downloaditems.downloads'); 
Route::get('admin/downloaditems/add','DownloadItemsController@create')->name('downloaditems.add');
Route::post('admin/downloaditems/add','DownloadItemsController@store')->name('downloaditems.add'); 
Route::get('admin/downloaditems/edit/{id}','DownloadItemsController@edit')->name('downloaditems.edit');
Route::get('admin/downloaditems/restore/{id}','DownloadItemsController@restore')->name('downloaditems.restore');
Route::get('admin/downloaditems/recycle/{id}','DownloadItemsController@recycle')->name('downloaditems.recycle');
Route::get('admin/downloaditems/profile/{id}','DownloadItemsController@show')->name('downloaditems.show');
Route::post('admin/downloaditems/edit/{id}','DownloadItemsController@update')->name('downloaditems.downloadedit'); 
Route::resource('downloaditems','DownloadItemsController');


//Admin Role Manage section routes 
Route::delete('deletealladmins','AdminManagerController@deleteAll');
Route::delete('restorealladmins','AdminManagerController@restoreall');
Route::delete('recycleAlladmins','AdminManagerController@recycleAll');
Route::get('admin/deletedadmins','AdminManagerController@showTrash')->name('manageadmins.deletedadmins'); 
Route::get('admin/manageadmins','AdminManagerController@index')->name('manageadmins.admins'); 
//Route::get('admin/downloaditems/add','DownloadItemsController@create')->name('downloaditems.add');
//Route::post('admin/downloaditems/add','DownloadItemsController@store')->name('downloaditems.add'); 
Route::get('admin/manageadmins/edit/{id}','AdminManagerController@edit')->name('manageadmins.edit');
//Route::get('admin/downloaditems/restore/{id}','DownloadItemsController@restore')->name('downloaditems.restore');
//Route::get('admin/downloaditems/recycle/{id}','DownloadItemsController@recycle')->name('downloaditems.recycle');
//Route::get('admin/downloaditems/profile/{id}','DownloadItemsController@show')->name('downloaditems.show');
Route::post('admin/manageadmins/edit/{id}','AdminManagerController@update')->name('manageadmins.adminedit'); 
Route::resource('manageadmins','AdminManagerController');

//staff section routes 
Route::delete('deleteallstaff','StaffController@deleteAll');
Route::delete('restoreallstaff','StaffController@restoreall');
Route::delete('recycleAllstaff','StaffController@recycleAll');
Route::get('admin/deletedstaff','StaffController@showTrash')->name('staff.deletedstaff'); 
Route::get('admin/staff','StaffController@index')->name('staff.staffmembers'); 
Route::get('admin/staff/add','StaffController@create')->name('staff.add');
Route::post('admin/staff/add','StaffController@store')->name('staff.add'); 
Route::get('admin/staff/edit','StaffController@edit')->name('staff.edit');
Route::get('admin/staff/restore/{id}','StaffController@restore')->name('staff.restore');
Route::get('admin/staff/recycle/{id}','StaffController@recycle')->name('staff.recycle');
Route::post('admin/staff/edit/{id}','StaffController@update')->name('staffs.update'); 
Route::resource('staff','StaffController');


//events section routes
Route::delete('restoreallevents','EventController@restoreall');
Route::delete('recycleAllevents','EventController@recycleAll');
Route::delete('deleteallevents','EventController@deleteAll');
Route::get('admin/deletedevents','EventController@showTrash')->name('event.deletedevents'); 
Route::get('admin/events','EventController@index')->name('event.events'); 
Route::get('admin/events/add','EventController@create')->name('event.add');
Route::post('admin/events/add','EventController@store')->name('event.add');
Route::get('admin/events/restore/{id}','EventController@restore')->name('event.restore');
Route::get('admin/events/recycle/{id}','EventController@recycle')->name('event.recycle');
Route::post('admin/events/show/{id}','EventController@show')->name('event.show');
Route::get('admin/events/edit','EventController@edit')->name('event.edit');
Route::post('admin/events/edit/{id}','EventController@update')->name('event.edit'); 
Route::get('admin/events/profile/{id}','EventController@show')->name('event.show');
Route::resource('events','EventController');


//news  section routes
Route::delete('restoreallnews','NewsController@restoreall');
Route::delete('recycleAllnews','NewsController@recycleAll');
Route::delete('deleteallnews','NewsController@deleteAll');
Route::get('admin/deletednews','NewsController@showTrash')->name('news.deletednews'); 
Route::get('admin/news','NewsController@index')->name('news.newses'); 
Route::get('admin/news/add','NewsController@create')->name('news.add');
Route::post('admin/news/add','NewsController@store')->name('news.add');
Route::post('admin/news/show/{id}','NewsController@show')->name('news.show');
Route::get('admin/news/edit','NewsController@edit')->name('news.edit');
Route::get('admin/news/restore/{id}','NewsController@restore')->name('news.restore');
Route::get('admin/news/recycle/{id}','NewsController@recycle')->name('news.recycle');
//Route::post('news/edit/{id}','NewsController@update')->name('news.edit'); 
Route::post('admin/news/edit/{id}','NewsController@update')->name('news.newsupdate'); 
Route::get('admin/news/profile/{id}','NewsController@show')->name('news.show');
Route::resource('news','NewsController');


//Workplace section routes 
Route::delete('deleteallworkplace','WorkplaceController@deleteAll');
Route::get('admin/workplaces','WorkplaceController@index')->name('workplace.workplaces'); 
Route::get('admin/workplaces/add','WorkplaceController@create')->name('workplace.add');
Route::post('admin/workplaces/add','WorkplaceController@store')->name('workplace.add'); 
Route::get('admin/workplaces/edit','WorkplaceController@edit')->name('workplace.edit');
//Route::get('admin/workplaces/profile/{id}','WorkplaceController@show')->name('workplace.show');
Route::post('admin/workplaces/edit/{id}','WorkplaceController@update')->name('workplace.edit'); 
Route::resource('workplaces','WorkplaceController');

//Workplace section routes 
Route::delete('deletealldepo','DepoController@deleteAll');
Route::get('admin/depos','DepoController@index')->name('depo.depos'); 
Route::get('admin/depos/add','DepoController@create')->name('depo.add');
Route::post('admin/depos/add','DepoController@store')->name('depo.add'); 
Route::get('admin/depos/edit','DepoController@edit')->name('depo.edit');
//Route::get('admin/workplaces/profile/{id}','WorkplaceController@show')->name('workplace.show');
Route::post('admin/depos/edit/{id}','DepoController@update')->name('depo.edit'); 
Route::resource('depos','DepoController');



//Designation section routes 
Route::delete('deletealldesignation','DesignationsController@deleteAll');
Route::get('admin/designations','DesignationsController@index')->name('designation.designations'); 
Route::get('admin/designations/add','DesignationsController@create')->name('designation.add');
Route::post('admin/designations/add','DesignationsController@store')->name('designation.add'); 
Route::get('admin/designations/edit','DesignationsController@edit')->name('designation.edit');
//Route::get('admin/workplaces/profile/{id}','WorkplaceController@show')->name('workplace.show');
Route::post('admin/designations/edit/{id}','DesignationsController@update')->name('designation.edit'); 
Route::resource('designations','DesignationsController');

//Group section routes 

Route::delete('restoreallgroups','GroupsController@restoreall');
Route::delete('recycleAllgroups','GroupsController@recycleAll');
Route::delete('deleteallgroups','GroupsController@deleteAll');
Route::get('admin/deletedgroups','GroupsController@showTrash')->name('groups.deletedgroups'); 
Route::get('admin/groups','GroupsController@index')->name('group.groups'); 
Route::get('admin/groups/add','GroupsController@create')->name('group.add');
Route::post('admin/groups/add','GroupsController@store')->name('group.add'); 
Route::get('admin/groups/edit','GroupsController@edit')->name('group.edit');
Route::get('admin/groups/restore/{id}','GroupsController@restore')->name('group.restore');
Route::get('admin/groups/recycle/{id}','GroupsController@recycle')->name('group.recycle');
Route::get('admin/groups/profile/{id}','GroupsController@show')->name('group.show');
Route::post('admin/groups/profile/addmember','GroupsController@savememberingroup')->name('group.addmemberingroup');
Route::post('admin/groups/edit/{id}','GroupsController@update')->name('group.edit'); 
Route::post('admin/groups/destroymember/','GroupsController@destroymember')->name('group.destroymember'); 
Route::post('admin/groups/destroymembers/','GroupsController@deleteAllmembers')->name('group.deleteallmembers'); 
Route::get('admin/groups/addmember/','GroupsController@showaddmemberform')->name('group.addmember'); 
Route::post('admin/groups/addmember/','GroupsController@savemember')->name('group.addmember'); 
Route::get('admin/searchmembergroup', ['as'=>'searchmembergroup','uses'=>'SmsController@searchResponse']);
Route::resource('groups','GroupsController');


//Message section routes 
//Route::delete('deleteall','SmsController@deleteAll');
Route::get('admin/messages','SmsController@index')->name('message.messages'); 
Route::get('admin/messages/singlesend','SmsController@createsinglemessage')->name('message.singlesend');
Route::post('admin/messages/singlesend','SmsController@storesinglemessage')->name('message.singlesend');
Route::get('admin/messages/sendall','SmsController@createmultiplemessage')->name('message.multiplesend');
Route::post('admin/messages/sendall','SmsController@storesmultiplemessage')->name('message.multiplesend');
Route::get('admin/messages/sendgroupmessage','SmsController@creategroupmessage')->name('message.groupsend');
Route::post('admin/messages/sendgroupmessage','SmsController@storesgroupmessage')->name('message.groupsend');
Route::get('admin/messages/sendmessageusers','SmsController@createmultiplemessageusers')->name('message.multiplesendusers');
Route::post('admin/messages/sendmessageusers','SmsController@storesmultiplemessageusers')->name('message.multiplesendusers');
//Route::post('admin/messages/send','SmsController@store')->name('message.send'); 
//Route::get('admin/messages/edit','SmsController@edit')->name('message.edit');
//Route::get('admin/messages/profile/{id}','SmsController@show')->name('message.show');
//Route::post('admin/messages/edit/{id}','SmsController@update')->name('message.edit'); 
//Route::get('admin/messages/addmember/','SmsController@showaddmemberform')->name('message.addmember'); 
//Route::post('admin/messages/addmember/','SmsController@savemember')->name('message.addmember'); 
Route::get('admin/searchmember', ['as'=>'searchmember','uses'=>'SmsController@searchResponse']);
Route::resource('messages','SmsController');









Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::get('admin/register', 'Admin\RegisterController@showRegistrationForm')->name('admin.register');
Route::post('admin/login', 'Admin\LoginController@login');
Route::post('admin/register', 'Admin\RegisterController@register')->name('admin.register');
//Route::post('admin', 'Admin\LoginController@logout');
Route::post('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/reset','Admin\ResetPasswordController@reset');
Route::get('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
//Route::post('register','Admin\RegisterController@register');
//Route::get('register','Admin\RegisterController@showRegistrationForm');


