<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registerPendingUsers extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'employee_id', 'union_id','isregistered',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   /* protected $hidden = [
        'password', 'remember_token',
    ];*/

}
